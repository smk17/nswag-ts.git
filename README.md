# nswnswaggerag-ts

#### 介绍

根据 swagger 文档生成 typescript 客户端调用代码

#### 安装教程

1.  npm i nswagger-ts -D

#### 使用说明

1. 在 package.json 的 scripts 节点增加 2 个执行命令

- "nswag-init":"nswag init", // 初始化配置
- "nswag-run":"nswag run" // 执行代码生成

- 第一步执行：npm run nswag-init 初始化项目
- 初始化完成后会在项目根目录创建文件夹 nswag,里面放置了配置文件及代码模板
- 修改配置文件和代码模板就可以进行第二步生成操作了

- config.js 为配置文件

  - 配置所有需要生成的接口，及相应的生成规则，详见 【配置参数】

- tpl 文件里面是代码模板(可以根据自己的实际情况调整模板)

  - base.ejs 接口调用基类模板，默认使用了 axios
  - method.ejs 接口函数生成模板
  - model.ejs 接口对象生成模板
  - mock.ejs 模拟数据代码生成模板-调用
  - mock-method.ejs 模拟数据代码生成模板-接口实例

- 第二步执行：npm run nswag-run 执行代码生成调用接口

#### 配置参数

``` js
{
  Name: 'nswagger-ts',
  Description: '根据swagger文档生成typescript客户端调用代码',
  Apis: [
    {
      SwaggerUrl: string // 接口文档地址（必填）
      ApiBase: string // 接口根节点（必填）
      ApiName: string // 接口名称（必填）
      OutPath: string // 输出目录（默认：项目根目录：/src/api/）
      TplPath: string // 模板路径（默认：初始化后会自动copy模板文件夹到项目根目录：nswag，如果进行了修改则需配置此目录）
      Mock: boolean // 是否启用模拟数据 （默认：false）
      FormatMock: Function // 接管模拟数据格式化
      FormatControllerName: Function // 格式化模块名称（默认：接口名称+Api）
      FormatMethodName: Function // 格式化接口名称（默认：小驼峰命名）
      FormatModelName: Function // 格式化dto对象、枚举名称（默认：只会去除特殊字符）
    }
  ]
}
```

##### 格式化函数示例

``` js
// 格式化模型
function formatModelName(n) {
  // TODO 预处理掉乱七八糟的字符
  return n.replace(/`1/g, '')
}

// 格式化模拟数据
function formatMock(val, p, mock) {
  switch (p.Type.TypeOf) {
    case 'string':
      switch (p.Name) {
        case 'name':
          val = '@cname'
          break
        case 'title':
          val = '@ctitle(10, 20)'
          break
        case 'mobile':
          val = '@natural(10000)'
          break
        case 'email':
          val = '@email'
          break
        case 'province':
          val = '@province'
          break
        case 'city':
          val = '@city'
          break
        case 'area':
          val = '@county'
          break
        default:
          val = '@ctitle(10, 20)'
          break
      }
      mock[p.Name] = val
      break
    case 'number':
      switch (p.Name) {
        case 'result_code':
          val = 0
          break
        case 'page_index':
          val = 1
          break
        case 'page_size':
          val = 15
          break
        case 'total_count':
          val = 30
          break
        default:
          val = '@integer(0, 100)'
          break
      }
      mock[p.Name] = val
      break
    case 'array':
      mock[p.Name + '|20'] = val
      break
  }
  return mock
}
```

#### 代码仓库

1. https://gitee.com/smk17/nswag-ts
2. https://www.npmjs.com/package/nswagger-ts
