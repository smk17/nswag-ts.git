/**
 * 初始化
 */
declare function init(args: string, isDebugger: boolean): Promise<void>;
export default init;
