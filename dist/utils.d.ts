import { NswagOptions } from './types';
/**
 * 生成
 * @param apiData 标准化数据
 * @param options 生成配置
 */
export default function build(options: NswagOptions): Promise<void>;
