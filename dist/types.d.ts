export interface NswagOptions {
    SwaggerUrl: string;
    ApiBase: string;
    ApiName: string;
    OutPath: string;
    TplPath: string;
    Mock: boolean;
    FormatMock: Function;
    FormatControllerName: Function;
    FormatMethodName: Function;
    FormatModelName: Function;
}
export interface ApiDataBaseInfo {
    Title: string;
    Description: string;
    Version: string;
}
export interface ApiController {
    Name: string;
    Description: string;
    Methods: Array<ApiMethod>;
    ImportModels: Array<string>;
}
export interface ApiMethod {
    Name: string;
    Url: string;
    Description: string;
    RequestName: string;
    Parameters: Array<Parameter>;
    ParametersQuery: Array<Parameter>;
    ParametersBody: Array<Parameter>;
    ParametersFormData: Array<Parameter>;
    ParametersHeader: Array<Parameter>;
    ParametersPath: Array<Parameter>;
    Responses: Type;
    MockData: any;
}
export interface Parameter {
    Name: string;
    CamelCaseName: string;
    Description: string;
    Required: boolean;
    Type: Type;
    In: 'query' | 'body' | 'formData' | 'header' | 'path';
    Default: any;
}
export interface Enum {
    Name: string;
    Description: string;
    Items: Array<EnumItem>;
}
export interface EnumItem {
    Name: string;
    Value: Number;
}
export interface Model {
    Name: string;
    Description: string;
    IsParameter: boolean;
    BaseModel: string;
    Properties: Array<Propertie>;
}
export interface Propertie {
    Name: string;
    Description: string;
    Type: Type;
}
export interface Type {
    TypeOf: 'string' | 'number' | 'boolean' | 'file' | 'array' | 'enum' | 'schema';
    TsType: string;
    Ref: any;
}
export interface ApiData {
    BaseInfo: ApiDataBaseInfo;
    Controllers: Array<ApiController>;
    Models: Array<Model>;
    Enums: Array<Enum>;
}
