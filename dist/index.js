"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var readline_1 = __importDefault(require("readline"));
var utils_1 = __importDefault(require("./utils"));
var path_1 = __importDefault(require("path"));
var fs_1 = __importStar(require("fs"));
var lodash_1 = __importDefault(require("lodash"));
/**
 * 生成进度
 * @param text 文字
 * @param step 进度
 * @param isOk 完成
 */
function renderProgress(text) {
    readline_1.default.cursorTo(process.stdout, 0, 1);
    readline_1.default.clearScreenDown(process.stdout);
    process.stdout.write("" + text);
}
/**
 * 复制
 * @param src 源地址
 * @param dst 目标地址
 */
function copy(src, dst) {
    //测试某个路径下文件是否存在
    if (!fs_1.default.existsSync(dst)) {
        //不存在
        fs_1.default.mkdir(dst, function () {
            //创建目录
            copy(src, dst);
        });
    }
    //读取目录
    fs_1.default.readdir(src, function (err, paths) {
        if (err) {
            throw err;
        }
        paths.forEach(function (path) {
            var _src = src + '/' + path;
            var _dst = dst + '/' + path;
            var readable;
            var writable;
            fs_1.stat(_src, function (err, st) {
                if (err) {
                    throw err;
                }
                if (st.isFile()) {
                    readable = fs_1.default.createReadStream(_src); //创建读取流
                    writable = fs_1.default.createWriteStream(_dst); //创建写入流
                    readable.pipe(writable);
                }
                else if (st.isDirectory()) {
                    //测试某个路径下文件是否存在
                    if (fs_1.default.existsSync(_dst)) {
                        //存在
                        copy(_src, _dst);
                    }
                    else {
                        //不存在
                        fs_1.default.mkdir(_dst, function () {
                            //创建目录
                            copy(_src, _dst);
                        });
                    }
                }
            });
        });
    });
}
/**
 * 设置默认参数
 * @param options 外部参数
 */
function defNswagOptions(options, isDebugger) {
    /**
     * 格式化模块名称（默认：接口名称+Api）
     * @param name 名称
     */
    function formatControllerName(name) {
        return name.indexOf('Api') !== -1 ? name : name + 'Api';
    }
    /**
     * 格式化接口名称（默认：小驼峰命名）
     * @param name 名称
     */
    function formatMethodName(name) {
        if (name === '/' || name === '') {
            return '';
        }
        var fnName = name.substring(name.lastIndexOf('/'));
        return lodash_1.default.camelCase(fnName);
    }
    /**
     * 格式化dto对象、枚举名称（默认：只会去除特殊字符）
     * @param name 名称
     */
    function formatModelName(name) {
        return name.replace(/[.,\[\]]/g, '');
    }
    /**
     * 格式化模拟值
     * @param v 默认格式化后的值
     * @param p 对应的属性
     */
    function formatMock(v, p) {
        return v;
    }
    var def = {
        SwaggerUrl: '',
        ApiBase: '',
        ApiName: '',
        OutPath: isDebugger ? '../test/src/api' : '../../../src/api',
        TplPath: isDebugger ? '../test/nswag/tpl' : '../../../nswag/tpl',
        Mock: false,
        FormatControllerName: formatControllerName,
        FormatMethodName: formatMethodName,
        FormatModelName: formatModelName,
        FormatMock: formatMock
    };
    return Object.assign(def, options);
}
/**
 * 初始化
 */
function init(args, isDebugger) {
    return __awaiter(this, void 0, void 0, function () {
        var srcPath, savePath, configs, configPath, config, i, config;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!(args === 'init')) return [3 /*break*/, 1];
                    srcPath = path_1.default.join(__dirname, '../def');
                    savePath = path_1.default.join(__dirname, isDebugger ? '../test/nswag' : '../../../nswag');
                    copy(srcPath, savePath);
                    console.log('初始化成功，您可以修改nswag文件夹下的config.js配置文件&代码模板,路径：' + savePath);
                    return [3 /*break*/, 6];
                case 1:
                    if (!(args === 'run')) return [3 /*break*/, 5];
                    configs = [];
                    configPath = path_1.default.join(__dirname, isDebugger ? '../test/nswag/config.js' : '../../../nswag/config.js');
                    if (fs_1.default.existsSync(configPath)) {
                        config = require(configPath);
                        if (config) {
                            configs = config.Apis;
                        }
                        else {
                            console.log('请先执行 nswag init 进行初始化2');
                            process.exit(0);
                        }
                    }
                    else {
                        console.log('请先执行 nswag init 进行初始化1');
                        process.exit(0);
                    }
                    i = 0;
                    _a.label = 2;
                case 2:
                    if (!(i < configs.length)) return [3 /*break*/, 4];
                    config = configs[i];
                    if (!config.ApiName) {
                        console.log('接口名称[ApiName]不能为空');
                        return [2 /*return*/];
                    }
                    if (!config.SwaggerUrl) {
                        console.log('接口地址[SwaggerUrl]不能为空');
                        return [2 /*return*/];
                    }
                    if (!config.ApiBase) {
                        console.log('接口根目录[ApiBase]不能为空');
                        return [2 /*return*/];
                    }
                    renderProgress("\u6B63\u5728\u751F\u6210 " + config.ApiName);
                    return [4 /*yield*/, utils_1.default(defNswagOptions(config, isDebugger))];
                case 3:
                    _a.sent();
                    renderProgress(config.ApiName + " \u751F\u6210\u6210\u529F");
                    i++;
                    return [3 /*break*/, 2];
                case 4:
                    process.exit(0);
                    return [3 /*break*/, 6];
                case 5:
                    console.log('请输入要执行的命名，查看帮助 nswag -h');
                    _a.label = 6;
                case 6: return [2 /*return*/];
            }
        });
    });
}
exports.default = init;
//# sourceMappingURL=index.js.map