"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __importDefault(require("./index"));
var packageInfo = require('../package.json');
var isDebugger = false;
if (process.argv.slice(3)) {
    console.log('已启用调试模式');
    isDebugger = process.argv.slice(3)[0] == 'dev';
}
function run(argv) {
    if (argv[0] === '-v' || argv[0] === '--version') {
        console.log("  version is " + packageInfo.version);
    }
    else if (argv[0] === '-h' || argv[0] === '--help') {
        console.log('  usage:\n');
        console.log('  nswag init [初始化]');
        console.log('  nswag run [执行生成]');
        console.log('  nswag -v --version [show version]');
    }
    else {
        index_1.default(argv[0], isDebugger);
    }
}
run(process.argv.slice(2));
//# sourceMappingURL=test.js.map