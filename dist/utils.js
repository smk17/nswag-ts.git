"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = __importDefault(require("axios"));
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var ejs_1 = __importDefault(require("ejs"));
var lodash_1 = __importDefault(require("lodash"));
var https_1 = __importDefault(require("https"));
/**
 * 获取Swagger的JSON数据
 * @param {*} swaggerUrl
 */
function getSwaggerData(swaggerUrl) {
    var agent = new https_1.default.Agent({
        rejectUnauthorized: false
    });
    return new Promise(function (resolve, reject) {
        axios_1.default.get(swaggerUrl, { httpsAgent: agent }).then(function (response) {
            if (response.status == 200) {
                var d = response.data;
                if (typeof d == 'string') {
                    var obj = eval('(' + d + ')');
                    resolve(obj);
                }
                else {
                    resolve(d);
                }
            }
            else {
                reject(new Error('获取swagger数据失败'));
            }
        });
    });
}
/**
 * 删除文件夹
 * @param path 地址
 */
function removeDirSync(path) {
    var files = [];
    /**
     * 判断给定的路径是否存在
     */
    if (fs_1.default.existsSync(path)) {
        /**
         * 返回文件和子目录的数组
         */
        files = fs_1.default.readdirSync(path);
        files.forEach(function (file) {
            var curPath = path_1.default.join(path, file);
            /**
             * fs.statSync同步读取文件夹文件，如果是文件夹，在重复触发函数
             */
            if (fs_1.default.statSync(curPath).isDirectory()) {
                // recurse
                removeDirSync(curPath);
            }
            else {
                fs_1.default.unlinkSync(curPath);
            }
        });
        /**
         * 清除文件夹
         */
        fs_1.default.rmdirSync(path);
    }
    else {
        console.log('给定的路径不存在，请给出正确的路径');
    }
}
/**
 * 创建目录
 * @param path 目录
 */
function markDirsSync(path) {
    try {
        if (!fs_1.default.existsSync(path)) {
            var pathtmp_1 = '';
            path.split(/[/\\]/).forEach(function (dirname) {
                // 这里指用/ 或\ 都可以分隔目录  如  linux的/usr/local/services   和windows的 d:\temp\aaaa
                if (pathtmp_1) {
                    pathtmp_1 = path_1.default.join(pathtmp_1, dirname);
                }
                else {
                    pathtmp_1 = dirname || '/';
                }
                if (!fs_1.default.existsSync(pathtmp_1)) {
                    fs_1.default.mkdirSync(pathtmp_1);
                }
            });
        }
        return true;
    }
    catch (e) {
        console.log('创建目录出错', e);
        return false;
    }
}
/**
 * 生成代码
 * @param tplPath 模板绝对地址
 * @param data 数据
 * @param outPath 文件存放绝对地址
 * @param fileName 文件名称
 */
function codeRender(tplPath, data, outPath, fileName) {
    if (markDirsSync(outPath)) {
        var fileText = ejs_1.default.render(fs_1.default.readFileSync(tplPath, 'utf-8'), data);
        var savePath = path_1.default.join(outPath, fileName);
        fs_1.default.writeFileSync(savePath, fileText);
    }
}
/**
 * 去掉换行
 * @param str 字符串
 */
function removeLineBreak(str) {
    return str ? str.replace(/[\r\n]/g, '') : '';
}
/**
 * 格式化模拟数据
 * @param properties 当前需要模拟的对象
 * @param models 接口全部对象
 * @param fm 格式化模拟数据函数
 * @param deep 递归层级，防止对象父子嵌套导致死循环 默认递归5级
 */
function formatMock(properties, models, fm, deep) {
    if (deep === void 0) { deep = 1; }
    var model = lodash_1.default.find(models, { Name: properties.Ref });
    if (model) {
        var mock_1 = {};
        model.Properties.forEach(function (p) {
            // 'string' | 'number' | 'boolean' | 'file' | 'array' | 'enum' | 'schema'
            var v = '';
            switch (p.Type.TypeOf) {
                case 'string':
                    v = '@ctitle(10, 20)';
                    break;
                case 'number':
                    v = '@integer(0, 100)';
                    break;
                case 'boolean':
                    v = '@boolean';
                    break;
                case 'file':
                    v = '';
                    break;
                case 'array':
                    v = deep > 5 ? [] : [formatMock(p.Type, models, fm, deep + 1)];
                    break;
                case 'enum':
                    v = p.Type.TsType[0];
                    break;
                case 'schema':
                    v = deep > 5 ? null : formatMock(p.Type, models, fm, deep + 1);
                    break;
            }
            mock_1 = fm(v, p, mock_1);
        });
        return mock_1;
    }
    else {
        return '';
    }
}
/**
 * 参数名称处理
 * @param {*} oldName
 */
function getParameterName(oldName) {
    var newName = oldName;
    // 关键词处理
    if (oldName === 'number') {
        newName = 'num';
    }
    if (oldName === 'string') {
        newName = 'str';
    }
    newName = lodash_1.default.camelCase(oldName);
    return newName;
}
/**
 * 处理重名问题
 * @param name 当前名称
 * @param list 列表，对象必须有Name属性才行
 */
function reName(name, list) {
    // 方法名称-重名处理
    if (lodash_1.default.findIndex(list, { Name: name }) !== -1) {
        var i = 1;
        while (true) {
            if (lodash_1.default.findIndex(list, { Name: name + '_' + i }) !== -1) {
                i++;
            }
            else {
                name = name + '_' + i;
                break;
            }
        }
    }
    return name;
}
/**
 * 格式化属性
 * @param properties 属性
 * @param options 配置
 */
function convertType(properties, options) {
    var type = {
        TypeOf: 'string',
        TsType: 'void',
        Ref: ''
    };
    if (!properties) {
        return type;
    }
    if (properties.hasOwnProperty('oneOf')) {
        return convertType(properties.oneOf[0], options);
    }
    if (properties.hasOwnProperty('allOf')) {
        return convertType(properties.allOf[0], options);
    }
    if (properties.hasOwnProperty('schema')) {
        return convertType(properties.schema, options);
    }
    if (properties.hasOwnProperty('$ref')) {
        var t = options.FormatModelName(properties.$ref.substring(properties.$ref.lastIndexOf('/') + 1));
        type = {
            TypeOf: 'schema',
            TsType: t,
            Ref: t
        };
    }
    else if (properties.hasOwnProperty('enum')) {
        type = {
            TypeOf: 'enum',
            TsType: properties.enum
                .map(function (item) {
                return JSON.stringify(item);
            })
                .join(' | '),
            Ref: ''
        };
    }
    else if (properties.type === 'array') {
        var iType = convertType(properties.items, options);
        type = {
            TypeOf: 'array',
            TsType: 'Array<' + iType.TsType + '>',
            Ref: iType.Ref
        };
    }
    else {
        type = {
            TypeOf: properties.type,
            TsType: '',
            Ref: ''
        };
        switch (properties.type) {
            case 'string':
                type.TypeOf = 'string';
                type.TsType = 'string';
                break;
            case 'number':
            case 'integer':
                type.TypeOf = 'number';
                type.TsType = 'number';
                break;
            case 'boolean':
                type.TypeOf = 'boolean';
                type.TsType = 'boolean';
                break;
            case 'file':
                type.TypeOf = 'file';
                type.TsType = 'string | Blob';
                break;
            default:
                type.TsType = 'any';
                break;
        }
    }
    return type;
}
/**
 * swagger 文档格式化
 * @param swagger
 * @param options
 */
function formatData(swagger, options) {
    // 文档模式 是否openapi 模式 还是 默认 swagger模式
    var isOpenApi = swagger.hasOwnProperty('openapi');
    var apiData = {
        BaseInfo: {
            Title: swagger.info.title,
            Description: swagger.info.description,
            Version: swagger.info.version // 接口版本号
        },
        Controllers: [],
        Models: [],
        Enums: []
    };
    // 格式化属性方法
    function fmProperties(properties, model) {
        lodash_1.default.forEach(properties, function (propertie, name) {
            var newp = {
                Name: name,
                Description: removeLineBreak(propertie.description),
                Type: convertType(propertie, options)
            };
            model.Properties.push(newp);
        });
    }
    // dto对象 / enum对象
    lodash_1.default.forEach(isOpenApi ? swagger.components.schemas : swagger.definitions, function (definition, name) {
        if (definition.hasOwnProperty('enum')) {
            var e_1 = {
                Name: options.FormatModelName(name),
                Description: removeLineBreak(definition.description),
                Items: []
            };
            var enums = lodash_1.default.zipObject(definition['x-enumNames'], definition.enum);
            lodash_1.default.forEach(enums, function (enumValue, enumName) {
                var item = {
                    Name: enumName,
                    Value: Number(enumValue)
                };
                e_1.Items.push(item);
            });
            apiData.Enums.push(e_1);
        }
        else {
            var m_1 = {
                Name: options.FormatModelName(name),
                Description: removeLineBreak(definition.description),
                IsParameter: false,
                BaseModel: '',
                Properties: []
            };
            // 格式化属性
            if (definition.hasOwnProperty('allOf')) {
                lodash_1.default.forEach(definition.allOf, function (propertie) {
                    if (propertie.hasOwnProperty('$ref')) {
                        m_1.BaseModel = options.FormatModelName(propertie.$ref.substring(propertie.$ref.lastIndexOf('/') + 1));
                    }
                    else {
                        if (propertie.hasOwnProperty('properties')) {
                            fmProperties(propertie.properties, m_1);
                        }
                    }
                });
            }
            else {
                fmProperties(definition.properties, m_1);
            }
            apiData.Models.push(m_1);
        }
    });
    // 模块
    lodash_1.default.mapKeys(swagger.ControllerDesc, function (value, key) {
        apiData.Controllers.push({
            Name: options.FormatControllerName(key),
            Description: removeLineBreak(value) || '接口太懒没写注释',
            Methods: [],
            ImportModels: []
        });
        return key;
    });
    // 方法
    lodash_1.default.forEach(swagger.paths, function (api, url) {
        lodash_1.default.forEach(api, function (md, requestName) {
            // 模块名称
            var cName = options.FormatControllerName(md.tags[0]);
            // 当前模块
            var currController = lodash_1.default.find(apiData.Controllers, { Name: cName });
            if (!currController) {
                // 没有就新加一个模块
                var newController = { Name: cName, Description: '接口太懒没写注释', Methods: [], ImportModels: [] };
                currController = newController;
                apiData.Controllers.push(currController);
            }
            // 方法名称
            var mName = options.FormatMethodName(url);
            mName = reName(mName, currController.Methods);
            // 添加方法
            var method = {
                Name: mName,
                Url: url,
                Description: removeLineBreak(md.summary) || '接口太懒没写注释',
                RequestName: requestName,
                Parameters: [],
                ParametersQuery: [],
                ParametersBody: [],
                ParametersFormData: [],
                ParametersHeader: [],
                ParametersPath: [],
                Responses: convertType(md.responses['200'] ? (isOpenApi ? md.responses['200'].content['application/json'].schema : md.responses['200'].schema) : null, options),
                MockData: null
            };
            // 方法参数处理
            // 兼容openapi 模式 requestBody 参数
            if (isOpenApi && md.requestBody) {
                md.parameters = md.parameters || [];
                md.parameters.push(Object.assign({
                    name: md.requestBody['x-name'],
                    required: md.requestBody.required,
                    in: 'body',
                    description: ''
                }, md.requestBody.content['application/json']));
            }
            lodash_1.default.forEach(md.parameters, function (parameter) {
                var pa = {
                    Name: parameter.name,
                    CamelCaseName: reName(getParameterName(parameter.name), method.Parameters),
                    Description: removeLineBreak(parameter.description),
                    In: parameter.in,
                    Required: parameter.required,
                    Default: '',
                    Type: convertType(parameter, options)
                };
                if (pa.In === 'query') {
                    method.ParametersQuery.push(pa);
                    method.Parameters.push(pa);
                }
                if (pa.In === 'body') {
                    method.ParametersBody.push(pa);
                    method.Parameters.push(pa);
                }
                if (pa.In === 'formData') {
                    method.ParametersFormData.push(pa);
                    method.Parameters.push(pa);
                }
                if (pa.In === 'header') {
                    method.ParametersHeader.push(pa);
                }
                if (pa.In === 'path') {
                    method.ParametersPath.push(pa);
                    method.Parameters.push(pa);
                }
                // 接口参数：存在引用型参数&没有没添加到引用列表的则添加到引用列表
                if (pa.Type.Ref && currController && currController.ImportModels.indexOf(pa.Type.Ref) == -1) {
                    currController.ImportModels.push(pa.Type.Ref);
                    // 标记为输入参数对象
                    var d = lodash_1.default.find(apiData.Models, { Name: pa.Type.Ref });
                    if (d) {
                        d.IsParameter = true;
                    }
                }
            });
            // 排序一下参数，把非必填参数排后面
            method.Parameters = lodash_1.default.orderBy(method.Parameters, ['Required'], ['asc']);
            // 返回值：存在引用型参数&没有没添加到引用列表的则添加到引用列表
            method.Responses.Ref && currController && currController.ImportModels.indexOf(method.Responses.Ref) == -1 && currController.ImportModels.push(method.Responses.Ref);
            // 返回值模拟
            if (options.Mock) {
                method.MockData = formatMock(method.Responses, apiData.Models, options.FormatMock);
            }
            // 添加方法
            currController.Methods.push(method);
        });
    });
    // 调整方法顺序，因为mock时 有可能匹配错误的mock拦截
    apiData.Controllers.map(function (c) {
        c.Methods = lodash_1.default.orderBy(c.Methods, ['Name'], ['desc']);
        return c;
    });
    // 清理无方法空模块
    lodash_1.default.remove(apiData.Controllers, function (c) {
        return c.Methods.length <= 0;
    });
    return apiData;
}
/**
 * 格式化成TS统一模板格式数据-数据源
 * @param swaggerUrl
 * @param options
 */
function getApiData(swaggerUrl, options) {
    return new Promise(function (resolve, reject) {
        getSwaggerData(swaggerUrl)
            .then(function (r) {
            var apiData = formatData(r, options);
            resolve(apiData);
        })
            .catch(function (e) {
            reject(e);
        });
    });
}
/**
 * 生成
 * @param apiData 标准化数据
 * @param options 生成配置
 */
function codeBuild(apiData, options) {
    var savePath = path_1.default.join(__dirname, options.OutPath, options.ApiName.toLowerCase());
    var saveMethodDir = savePath;
    var saveBaseDir = path_1.default.join(savePath, 'base');
    var saveModelsDir = path_1.default.join(savePath, 'model');
    var saveMockDir = path_1.default.join(savePath, 'mock');
    var tplPath = path_1.default.join(__dirname, options.TplPath);
    var tplBasePath = path_1.default.join(tplPath, 'base.ejs');
    var tplMethodPath = path_1.default.join(tplPath, 'method.ejs');
    var tplModelsPath = path_1.default.join(tplPath, 'model.ejs');
    var tplMockPath = path_1.default.join(tplPath, 'mock.ejs');
    var tplMockMethodPath = path_1.default.join(tplPath, 'mock-method.ejs');
    // 清理旧代码
    removeDirSync(savePath);
    // 生成-基类
    codeRender(tplBasePath, { apiData: apiData, options: options }, saveBaseDir, 'index.ts');
    // 生成-dto对象
    codeRender(tplModelsPath, { apiData: apiData, options: options }, saveModelsDir, 'index.ts');
    // 生成-Mock-调用
    if (options.Mock) {
        codeRender(tplMockPath, { apiData: apiData, options: options }, saveMockDir, 'index.ts');
    }
    // 按模块生成接口
    apiData.Controllers.forEach(function (controller) {
        var controllerName = options.FormatControllerName(controller.Name);
        // 生成-接口
        codeRender(tplMethodPath, { controller: controller, options: options }, saveMethodDir, controllerName + '.ts');
        // 生成-Mock-接口
        if (options.Mock) {
            codeRender(tplMockMethodPath, { controller: controller, options: options }, saveMockDir, controllerName + '.ts');
        }
    });
}
/**
 * 生成
 * @param apiData 标准化数据
 * @param options 生成配置
 */
function build(options) {
    return __awaiter(this, void 0, void 0, function () {
        var apiData;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, getApiData(options.SwaggerUrl, options)];
                case 1:
                    apiData = _a.sent();
                    codeBuild(apiData, options);
                    return [2 /*return*/];
            }
        });
    });
}
exports.default = build;
//# sourceMappingURL=utils.js.map